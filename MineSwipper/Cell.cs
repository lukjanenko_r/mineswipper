﻿using System.Drawing;

namespace MineSwipper
{
    class Cell
    {
        public bool isMine;
        public bool isFlag;
        public bool isPushed;
        public bool isRecursionCancel;
        public bool isCounted;
        public bool isBoom;
        public bool isIncorrectFlag;

        public int minesBeside;

        public Brush colorFigure;

        public int x;
        public int y;

        public int size;

        public Rectangle getRect()
        {
            Rectangle rect = new Rectangle(x, y, size, size);
            return rect;
        }

        public Rectangle getRectForMine()
        {
            Rectangle rect = new Rectangle(x + size / 4, y + size / 4, size / 2, size / 2);
            return rect;
        }

        public Point[] getLinesForMine()
        {
            Point[] points = new Point[5];
            points[0] = new Point(x + size / 8, y + size / 2);
            points[1] = new Point(x + size - size / 8, y + size / 2);
            points[2] = new Point(x + size / 2, y + size / 2);
            points[3] = new Point(x + size / 2, y + size / 8);
            points[4] = new Point(x + size / 2, y + size - size / 8);
            return points;
        }

        public Rectangle[] getRectsForFlag()
        {
            Rectangle[] rects = new Rectangle[2];
            rects[0] = new Rectangle(x + size / 8, y + size / 8, size * 10 / 100, size * 80 / 100);
            rects[1]= new Rectangle(x + size / 5, y + size / 8, size / 2, size / 3);
            return rects;
        }

        public Point[] getWhitePoints()
        {
            Point[] points = new Point[3];
            if (!isPushed)
            {
                points[0] = new Point(x, y + size);
                points[1] = new Point(x, y);
                points[2] = new Point(x + size, y);
            }
            else
            {
                points[0] = new Point(x, y + size);
                points[1] = new Point(x + size, y + size);
                points[2] = new Point(x + size, y);
            }
            return points;
        }

        public Point[] getBlackPoints()
        {
            Point[] points = new Point[3];
            if (!isPushed)
            {
                points[0] = new Point(x, y + size);
                points[1] = new Point(x + size, y + size);
                points[2] = new Point(x + size, y);
            }
            else
            {
                points[0] = new Point(x, y + size);
                points[1] = new Point(x, y);
                points[2] = new Point(x + size, y);
            }
            return points;
        }

        public void getColorFigure()
        {
            switch (minesBeside)
            {
                case 0:
                    colorFigure = Brushes.Silver;
                    break;
                case 1:
                    colorFigure = Brushes.Blue;
                    break;
                case 2:
                    colorFigure = Brushes.Green;
                    break;
                case 3:
                    colorFigure = Brushes.Red;
                    break;
                case 4:
                    colorFigure = Brushes.BlueViolet;
                    break;
                case 5:
                    colorFigure = Brushes.Brown;
                    break;
                case 6:
                    colorFigure = Brushes.DarkRed;
                    break;
                case 7:
                    colorFigure = Brushes.Sienna;
                    break;
                case 8:
                    colorFigure = Brushes.Black;
                    break;
                default:
                    colorFigure = Brushes.Silver;
                    break;
            }
        }
    }
}
