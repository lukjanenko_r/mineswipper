﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace MineSwipper
{
    public partial class Form1 : Form
    {
        int minesQuantity = 10;                                     //количество мин
        const int FIELD_SIZE = 9;                                   //размерность поля

        int cellSize = 20;                                          //размер ячейки в пикселях

        int counter;

        Cell[,] cells = new Cell[FIELD_SIZE, FIELD_SIZE];           //массив ячеек на поле

        StartButton sb = new StartButton();

        bool isFirstClick;

        int fieldSize;
        int indent = 2;
        int minesLeft;

        Stopwatch stopWatch = new Stopwatch();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            fieldSize = FIELD_SIZE;
            cells = (Cell[,])resizeArray(cells, new Cell[fieldSize, fieldSize]);
            newGame();
        }

        private Array resizeArray(Array arr, Cell[,] newSizes)
        {
            var temp = Array.CreateInstance(arr.GetType().GetElementType(), fieldSize, fieldSize);
            int length = arr.Length <= temp.Length ? arr.Length : temp.Length;
            Array.ConstrainedCopy(arr, 0, temp, 0, length);
            return temp;
        }

        private void newGame()
        {
            label4.Text = "00:00";
            stopWatch.Stop();
            stopWatch.Reset();
            timer1.Enabled = true;

            sb.isLose = false;
            sb.isWin = false;

            Size = new Size(fieldSize * (cellSize + indent) + 18 + indent,
                            fieldSize * (cellSize + indent) + 115 + indent);

            for (int i = 0; i <= cells.GetUpperBound(0); i++)
            {
                for (int j = 0; j <= cells.GetUpperBound(0); j++)
                {
                    cells[i, j] = new Cell();
                    cells[i, j].x = indent + i * (cellSize + indent);
                    cells[i, j].y = indent + j * (cellSize + indent) + 75;

                    cells[i, j].size = cellSize;
                    cells[i, j].isPushed = false;
                    cells[i, j].isRecursionCancel = false;
                    cells[i, j].isCounted = false;
                    cells[i, j].isBoom = false;
                    cells[i, j].isIncorrectFlag = false;
                    cells[i, j].isMine = false;
                    cells[i, j].isFlag = false;
                    cells[i, j].minesBeside = 0;
                }
            }
            counter = fieldSize * fieldSize - minesQuantity;
            isFirstClick = true;
            minesLeft = minesQuantity;

            Invalidate();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g1 = e.Graphics;

            Bitmap bm = new Bitmap(fieldSize * (cellSize + indent) + 18 + indent,
                            fieldSize * (cellSize + indent) + 75 + indent,
                            g1);

            Graphics g = Graphics.FromImage(bm);

            g.DrawLines(Pens.White, sb.getWhitePoints());
            g.DrawLines(Pens.Black, sb.getBlackPoints());
            g.FillEllipse(Brushes.Yellow, sb.getFaceRect());
            g.FillEllipse(Brushes.Black, sb.getLeftEye());
            g.FillEllipse(Brushes.Black, sb.getRightEye());
            if (!sb.isLose) g.DrawArc(Pens.Black, sb.getSmile(), 0, 180);
            else g.DrawArc(Pens.Black, sb.getSmile(), 180, 180);

            for (int i = 0; i <= cells.GetUpperBound(0); i++)
            {
                for (int j = 0; j <= cells.GetUpperBound(0); j++)
                {
                    if (cells[i, j].isPushed == true)
                    {
                        if (!cells[i, j].isMine)
                        {
                            Rectangle rect = cells[i, j].getRect();
                            g.FillRectangle(Brushes.Silver, rect);
                            g.DrawRectangle(Pens.Black, rect);

                            Font font = new Font("Times New Roman", 14.0f);
                            font = new Font("Arial", 11.0f, FontStyle.Bold);

                            g.DrawString(cells[i, j].minesBeside.ToString(),
                                font,
                                cells[i, j].colorFigure,
                                cells[i, j].x + cellSize / 7.0f,
                                cells[i, j].y + cellSize / 20.0f);
                        }
                        else
                        {
                            if (cells[i, j].isBoom) g.FillRectangle(Brushes.Red, cells[i, j].getRect());
                            g.FillEllipse(Brushes.Black, cells[i, j].getRectForMine());
                            g.DrawLines(Pens.Black, cells[i, j].getLinesForMine());

                            g.DrawLines(Pens.Black, cells[i, j].getWhitePoints());
                            g.DrawLines(Pens.Black, cells[i, j].getBlackPoints());
                        }
                    }
                    else
                    {
                        g.DrawLines(Pens.White, cells[i, j].getWhitePoints());
                        g.DrawLines(Pens.Black, cells[i, j].getBlackPoints());

                        if (cells[i, j].isFlag == true)
                        {
                            Rectangle[] rect = new Rectangle[2];
                            rect = cells[i, j].getRectsForFlag();
                            g.FillRectangle(Brushes.Black, rect[0]);
                            g.FillRectangle(Brushes.Red, rect[1]);
                        }
                    }

                    if (cells[i, j].isIncorrectFlag)
                    {
                        g.DrawLine(Pens.Red, cells[i, j].x, cells[i, j].y, cells[i, j].x + cellSize, cells[i, j].y + cellSize);
                        g.DrawLine(Pens.Red, cells[i, j].x + cellSize, cells[i, j].y, cells[i, j].x, cells[i, j].y + cellSize);
                    }
                }
            }

            g1.DrawImage(bm, 0, 0);
        }

        private void mineMix()
        {
            isFirstClick = false;
            for (int i = 0; i < minesQuantity; i++)
            {
                Random r = new Random(DateTime.Now.Millisecond);
                int a = r.Next(0, fieldSize);
                int b = r.Next(0, fieldSize);

                if (!cells[a, b].isPushed && !cells[a, b].isMine) cells[a, b].isMine = true;
                else i--;
            }
            minesBesideCulculation();
        }

        private void minesBesideCulculation()
        {
            for (int i = 0; i <= cells.GetUpperBound(0); i++)
            {
                for (int j = 0; j <= cells.GetUpperBound(0); j++)
                {
                    if ((i - 1 >= 0) && (j - 1 >= 0))
                        if (cells[i - 1, j - 1].isMine) cells[i, j].minesBeside++;
                    if (j - 1 >= 0)
                        if (cells[i, j - 1].isMine) cells[i, j].minesBeside++;
                    if ((i + 1 < fieldSize) && (j - 1 >= 0))
                        if (cells[i + 1, j - 1].isMine) cells[i, j].minesBeside++;

                    if (i - 1 >= 0)
                        if (cells[i - 1, j].isMine) cells[i, j].minesBeside++;
                    if (i + 1 < fieldSize)
                        if (cells[i + 1, j].isMine) cells[i, j].minesBeside++;

                    if ((i - 1 >= 0) && (j + 1 < fieldSize))
                        if (cells[i - 1, j + 1].isMine) cells[i, j].minesBeside++;
                    if (j + 1 < fieldSize)
                        if (cells[i, j + 1].isMine) cells[i, j].minesBeside++;
                    if ((i + 1 < fieldSize) && (j + 1 < fieldSize))
                        if (cells[i + 1, j + 1].isMine) cells[i, j].minesBeside++;

                    cells[i, j].getColorFigure();
                }
            }
        }

        private void push(int i, int j)
        {
            if ((i >= 0) && (i < fieldSize) && (j >= 0) && (j < fieldSize))
            {
                cells[i, j].isPushed = true;
                if (cells[i, j].isFlag)
                {
                    minesLeft++;
                    cells[i, j].isFlag = false;
                }

                if (!cells[i, j].isCounted)
                {
                    cells[i, j].isCounted = true;
                    counter--;
                }
                if (cells[i, j].minesBeside == 0 && !cells[i, j].isRecursionCancel) pushNeighboringCells(i, j);
            }
        }

        private void pushNeighboringCells(int i, int j)
        {
            cells[i, j].isRecursionCancel = true;

            push(i - 1, j - 1);
            push(i - 1, j);
            push(i - 1, j + 1);

            push(i + 1, j - 1);
            push(i + 1, j);
            push(i + 1, j + 1);

            push(i, j - 1);
            push(i, j + 1);
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            if (sb.getRect().Contains(e.Location))
                newGame();
            for (int i = 0; i <= cells.GetUpperBound(0); i++)
            {
                bool isExit = false;
                for (int j = 0; j <= cells.GetUpperBound(0); j++)
                {
                    if (cells[i, j].getRect().Contains(e.Location))
                    {
                        if (e.Button == MouseButtons.Left && !cells[i, j].isFlag && !cells[i, j].isPushed && sb.isStepPossible())
                        {
                            cells[i, j].isPushed = true;
                            cells[i, j].isFlag = false;
                            if (!cells[i, j].isCounted)
                            {
                                counter--;
                                cells[i, j].isCounted = true;
                            }

                            if (isFirstClick)
                            {
                                mineMix();
                                stopWatch.Start();
                            }

                            if (cells[i, j].isMine)
                            {
                                cells[i, j].isBoom = true;

                                for (int a = 0; a <= cells.GetUpperBound(0); a++)
                                {
                                    for (int b = 0; b <= cells.GetUpperBound(0); b++)
                                    {
                                        if (cells[a, b].isMine && !cells[a, b].isFlag)
                                        {
                                            cells[a, b].isPushed = true;
                                        }
                                        if (!cells[a, b].isMine && cells[a, b].isFlag)
                                        {
                                            cells[a, b].isIncorrectFlag = true;
                                            cells[a, b].isFlag = false;
                                        }
                                    }
                                }

                                Invalidate();

                                stopWatch.Stop();
                                sb.isLose = true;
                                timer1.Enabled = false;
                                stopWatch.Reset();

                                counter++;
                            }
                            else
                            {
                                if (cells[i, j].minesBeside == 0)
                                {
                                    pushNeighboringCells(i, j);
                                }
                            }
                            Invalidate();
                            isExit = true;
                            break;
                        }

                        if (e.Button == MouseButtons.Right && !cells[i, j].isPushed && sb.isStepPossible())
                        {
                            if (cells[i, j].isFlag)
                            {
                                cells[i, j].isFlag = false;
                                minesLeft++;
                            }
                            else
                            {
                                cells[i, j].isFlag = true;
                                minesLeft--;
                            }

                            Invalidate();

                            isExit = true;
                            break;
                        }
                    }
                }
                if (isExit) break;
            }
            isWin();
        }

        private void isWin()
        {
            if (counter == 0)
            {
                for (int i = 0; i <= cells.GetUpperBound(0); i++)
                {
                    for (int j = 0; j <= cells.GetUpperBound(0); j++)
                    {
                        if (!cells[i, j].isFlag) cells[i, j].isPushed = true;
                    }
                }
                minesLeft = 0;
                stopWatch.Stop();
                timer1.Enabled = false;
                sb.isWin = true;
                stopWatch.Reset();
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void changeSize(int fs, int mq)
        {
            fieldSize = fs;
            minesQuantity = mq;
            cells = (Cell[,])resizeArray(cells, new Cell[fieldSize, fieldSize]);
            newGame();
        }

        private void easyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            changeSize(9, 10);
        }

        private void mediumToolStripMenuItem_Click(object sender, EventArgs e)
        {
            changeSize(16, 40);
        }

        private void hardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            changeSize(22, 80);
        }

        TextBox tb1 = new TextBox();
        TextBox tb2 = new TextBox();
        Form dialog = new Form();

        private void userToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Visible = false;

            dialog.Size = new Size(375, 120);
            dialog.StartPosition = FormStartPosition.CenterScreen;
            dialog.Text = "Please change size and mine`s quality:";
            dialog.MinimizeBox = false;
            dialog.MaximizeBox = false;
            dialog.ControlBox = false;

            Button b1 = new Button();
            Button b2 = new Button();
            b1.Size = new Size(70, 25);
            b2.Size = new Size(70, 25);
            b1.Location = new Point(275, 10);
            b2.Location = new Point(275, 50);
            b1.Text = "Ok";
            b2.Text = "Cancel";

            b1.Click += new EventHandler(button1_Click);
            b2.Click += new EventHandler(button2_Click);

            Label l1 = new Label();
            Label l2 = new Label();
            l1.Location = new Point(15, 15);
            l2.Location = new Point(15, 55);
            l1.Size = new Size(150, 20);
            l2.Size = new Size(150, 20);
            l1.Text = "Enter cell`s quality abreast:";
            l2.Text = "Enter mine`s quality:";
            
            tb1.Location = new Point(170, 12);
            tb2.Location = new Point(170, 52);
            tb1.Size = new Size(30, 20);
            tb2.Size = new Size(30, 20);
            tb1.Text = fieldSize.ToString();
            tb2.Text = minesQuantity.ToString();

            dialog.Visible = true;
            dialog.Show();
            dialog.Controls.Add(b1);
            dialog.Controls.Add(b2);
            dialog.Controls.Add(l1);
            dialog.Controls.Add(l2);
            dialog.Controls.Add(tb1);
            dialog.Controls.Add(tb2);
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if ((int.Parse(tb1.Text) >= 9) && (int.Parse(tb2.Text) > 0) && (int.Parse(tb1.Text) * int.Parse(tb1.Text) > int.Parse(tb2.Text)))
                {
                    fieldSize = int.Parse(tb1.Text);
                    minesQuantity = int.Parse(tb2.Text);
                    changeSize(fieldSize, minesQuantity);
                    dialog.Visible = false;
                    Visible = true;
                    Activate();
                }
                else
                {
                    MessageBox.Show("Please enter correct values.\n\nCells abreast should be more than 8, quality of mines should be less than total quality of cells also.");
                }
            }
            catch
            {
                MessageBox.Show("Please enter correct values.\n\nCells abreast should be more than 8, quality of mines should be less than total quality of cells also.");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            dialog.Visible = false;
            Visible = true;
            Activate();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label3.Text = minesLeft.ToString();

            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}",
                                ts.Minutes, ts.Seconds);

            label4.Text = elapsedTime;
        }
    }
}
