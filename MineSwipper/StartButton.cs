﻿using System.Drawing;

namespace MineSwipper
{
    class StartButton
    {
        public bool isLose = false;
        public bool isWin = false;

        public int x = 158;
        public int y = 30;

        public int size = 39;

        public Rectangle getRect()
        {
            Rectangle rect = new Rectangle(x, y, size, size);
            return rect;
        }
        
        public Rectangle getSmile()
        {
            Rectangle rect;

            if ((!isLose) && (!isWin))
            {
                rect = new Rectangle(x + size / 3, y + size - size / 3, size / 3, size / 20);
            }
            else
            {
                if (isWin) rect = new Rectangle(x + size / 3, y + size / 2, size / 3, size / 5);
                else rect = new Rectangle(x + size / 3, y + size - size / 3, size / 3, size / 5);
            }

            return rect;
        }

        public Rectangle getFaceRect()
        {
            Rectangle rect = new Rectangle(x + size / 8, y + size / 8, size - size / 4, size - size / 4);
            return rect;
        }

        public Rectangle getLeftEye()
        {
            Rectangle rect = new Rectangle(x + size / 3, y + size / 3, size / 6, size / 6);
            return rect;
        }

        public Rectangle getRightEye()
        {
            Rectangle rect = new Rectangle(x + size / 3 + size / 4, y + size / 3, size / 6, size / 6);
            return rect;
        }

        public Point[] getWhitePoints()
        {
            Point[] points = new Point[3];
            points[0] = new Point(x, y + size);
            points[1] = new Point(x, y);
            points[2] = new Point(x + size, y);
            return points;
        }

        public Point[] getBlackPoints()
        {
            Point[] points = new Point[3];
            points[0] = new Point(x, y + size);
            points[1] = new Point(x + size, y + size);
            points[2] = new Point(x + size, y);
            return points;
        }

        public bool isStepPossible()
        {
            return !isWin && !isLose;
        }
    }
}
